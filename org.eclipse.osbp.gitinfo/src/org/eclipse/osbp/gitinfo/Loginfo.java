/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.gitinfo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class Loginfo implements logging of git information created during the build process.
 * 
 * With this information it will be possible to identify the corresponding source code in the internal git archive, 
 * even after a transport through different archives.
 *  
 * gitbase.properties: holds the information about the parent repo
 * git.properties: holds the information about the build repo
 * 
 */
public class Loginfo {

	/** The logger. */
	private static Logger LOGGER = LoggerFactory.getLogger(Loginfo.class); // NOSONAR

	/**
	 * Instantiates a new loginfo.
	 */
	public Loginfo() {
		// nothing to do in constructor
	}
	
	/**
	 * Prints the loginfo.
	 *
	 * @param cn the ClassName
	 * @param cl the ClassLoader
	 */
	public void print( String cn, ClassLoader cl  ) {
		printLoginfo( LOGGER, cn, cl );
	}
	
	/**
	 * Prints the.
	 *
	 * @param extLOGGER the external logger
	 * @param cn the ClassName
	 * @param cl the ClassLoader
	 */
	public void print( Logger extLOGGER, String cn, ClassLoader cl  ) {
		printLoginfo(extLOGGER, cn, cl );
	}
	
	/**
	 * Mylog.
	 *
	 * @param extLOGGER the ext logger
	 * @param infoText the info text
	 */
	private void mylog( Logger extLOGGER, String infoText) {
		if (extLOGGER != null) {
			try {
				extLOGGER.info(infoText);
			}
			catch(Exception ex) {// NOSONAR
				System.out.println(infoText); // NOSONAR
			}
		}
		else {
			System.out.println(infoText); // NOSONAR
		}
	}

	
	
/**
 * Prints the loginfo.
 *
 * @param extLOGGER the ext logger
 * @param cn the ClassName
 * @param cl the ClassLoader
 * @param propFile the properties file containing the git properties
 * @param prefixInfoText the prefix info text
 * @return true, if successful
 */
private boolean printOneLoginfo( Logger extLOGGER, String cn, ClassLoader cl, String  propFile, String	prefixInfoText) {
	boolean bSuccess = true;
	Properties prop = new Properties();
	InputStream input = null;

	String	infoText = cn + ": " + prefixInfoText + " ";
	
	try {

		input = cl.getResourceAsStream(propFile);
		// load a properties file
		if ( input != null ) {
			prop.load(input);
			infoText =  infoText + 
					   "code based on branch " + prop.getProperty("git.branch") + " " + 
					   "with id " + prop.getProperty("git.commit.id") + " " +
					   "dated " + prop.getProperty("git.commit.time");
			mylog(extLOGGER, infoText);
			String remoteurl = prop.getProperty("git.remote.origin.url");
			if ( !remoteurl.isEmpty() ) {
			
				String host = remoteurl.substring(remoteurl.indexOf('@')+1);
				
				if ( host.isEmpty() ) {
					host = "unknownhost";
				} else {
					host = host.substring(0,host.indexOf('/'));
				}

				String gitrepo = remoteurl.substring(remoteurl.lastIndexOf('/')+1);
				if ( gitrepo.isEmpty() ) {
					gitrepo = "unknown-repo.git";
				}
				if ( "Development".equals(prefixInfoText) ) {
					infoText = "last commit -> http://"+host+"/trac/changeset/"+prop.getProperty("git.commit.id")+"/"+gitrepo.substring(0,gitrepo.indexOf(".git"));
				}
			    mylog(extLOGGER, infoText);
			}
		}
		else {
			infoText = cn + ": "+propFile+" not found, continue ...";
			mylog(LOGGER, infoText);
			bSuccess = false;
		}

	} catch (Exception ex) { 
		infoText = cn + ": "+propFile+" not found, continue ...";
		LOGGER.debug(infoText);
		LOGGER.error(ex.toString()); 
		bSuccess = false;
	} finally {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) { 
				LOGGER.error(e.toString()); 
			}
		}
	}
	return bSuccess;
}
	
	
	
/**
 * Prints the loginfo.
 *	fill parameters like this		
 *	<code>ClassLoader cl = Activator.class.getClassLoader();</code>
 *
 * @param extLOGGER the ext logger
 * @param cn the ClassName
 * @param cl the ClassLoader
 */
	private void printLoginfo(Logger extLOGGER, String cn, ClassLoader cl ) {

//		first try current implementation, then try previous
		if ( !printOneLoginfo( extLOGGER, cn, cl, "META-INF/gitbase.properties", "Development" ) ) {
			printOneLoginfo( extLOGGER, cn, cl, "gitbase.properties", "Development" );		
		}
		
//		first try current implementation, then try previous
		if ( !printOneLoginfo( extLOGGER, cn, cl, "META-INF/git.properties", "Build" ) ) {
			printOneLoginfo( extLOGGER, cn, cl, "git.properties", "Build" );		
		}
		
	}
}


